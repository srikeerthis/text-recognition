import cv2

# Take a photo of text as user input
def takeSnapshot():
    camera = cv2.VideoCapture(0)
    img_name = ''

    while True:
        ret, frame = camera.read()
        if not ret:
            print("failed to take image")
            break
        # control brightness and contrast
        cv2.normalize(frame, frame, 0, 255, cv2.NORM_MINMAX)
        # opens camera window
        cv2.imshow("Take sample input", frame)
        
        # Keyboard input
        keyboardkey = cv2.waitKey(1)
        if keyboardkey % 256 == 27:
            # ESC pressed exit camera
            print("Closing...")
            break
        elif keyboardkey % 256 == 32:
            # Space key will take photo 
            img_name = "./assets/camera_sample.png"
            cv2.imwrite(img_name, frame)
            print("image taken")
            break

    camera.release()
    cv2.destroyAllWindows()
    return img_name
