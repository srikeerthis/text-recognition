# Text recognition using pytesseract library

This application will recognize text from image file or image taken from a camera.

### Steps to execute the application

- Clone the repository
- Install build requirements

```
cd text-recognition
pip install -r requirements.txt
```

- Run the application

```
python3 textrecognition.py
```
- Two options are available to add image
	-  via camera 
	- by selecting a file from assets folder 
- Open output.txt for text recognized


> Note:
 The accuracy of the text prediction taken from the camera is very low as the image requires more preprocessing
