import cv2
import pytesseract
import numpy as np
import camera
import fileopen

# get grayscale image
def get_grayscale(image):
    return cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

# noise removal
def remove_noise(image):
    return cv2.medianBlur(image, 5)

# thresholding
def thresholding(image):
    return cv2.threshold(image, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)[1]

choice = input(
    "Select the option of image input \n 1. Camera \n 2. Select file \n")
if choice == '1':
    try:
        snap = camera.takeSnapshot()
        image = cv2.imread(snap)
        gray = get_grayscale(image)
        noise = remove_noise(gray)
        thresh = thresholding(gray)
    except Exception as e:
        print("Error in processing image", e)
else:
    try:
        selectimage = fileopen.selectFile()
        assert(selectimage.lower().endswith(('.png', '.jpg', '.jpeg')))
        image = cv2.imread(selectimage)
        gray = get_grayscale(image)
        noise = remove_noise(gray)
        thresh = thresholding(gray)
    except Exception as e:
        print("Error as file is not selected", e)
try:
    # process the image to recognize string
    text = pytesseract.image_to_string(thresh)
    # Store text in file
    fileobj = open("output.txt","w")
    fileobj.write(text)
    fileobj.close()
    print("digitized output is present in output.txt")
except:
    print("text processing was not performed")