import tkinter as tk
from tkinter import filedialog

def selectFile():
    # Open window for file selection
    root = tk.Tk()
    root.withdraw()
    # store the path of file
    file_path = filedialog.askopenfilename()
    return file_path
